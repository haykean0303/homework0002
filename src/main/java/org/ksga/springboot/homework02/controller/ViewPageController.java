package org.ksga.springboot.homework02.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ViewPageController {
    @GetMapping("/view")
    public String person(){
        System.out.println("View Page");
        return "ViewPage";
    }
}
