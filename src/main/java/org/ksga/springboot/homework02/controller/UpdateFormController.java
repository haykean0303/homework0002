package org.ksga.springboot.homework02.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class UpdateFormController {
    @GetMapping("/update")
    public String update(){
        System.out.println("Update Form");
        return "UpdateForm";
    }
}
