package org.ksga.springboot.homework02.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class NewArticleController {
    @GetMapping("/article")
    public String article(){
        System.out.println("Article");
        return "NewArticle";
    }
}
